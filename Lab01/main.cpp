#include<iostream>
#include<fstream>
using namespace std;
// decleration of the functions
float sum(int*, int);
float average(int*, int);
float product(int*, int);
float smallest(int*, int);
int main()
{
	//! file openning
	fstream Inputfile;
	Inputfile.open("input.txt", ios::in);
	//! if input.txt file is not exits, the code will enter this statement and program is going to be terminated.
	if (!Inputfile) {
		cout << "Error!File can not be opened!";
		exit(0);
	}
	//! declerations of variables.
	int* values, number_value, counter = 0;
	//! reading the first integer from text file, which is the number of element in the file.
	Inputfile >> number_value;
	//! opening space at memory according to number_value
	values = new int[number_value];
	//!reading numbers that is calculated from input file
	while (!Inputfile.eof()) {
		Inputfile >> values[counter];
		counter++;
	}
	/**
	* if the value number_value is not equal to the number of numbers which was read from the file,
	* program will be finished.
	*/
	if (counter != number_value) {
		cout << "Error!" << endl;
		exit(0);
	}
	//! Printing results.
	cout << "Sum: " << sum(values, number_value) << endl;
	cout << "Average: " << average(values, number_value) << endl;
	cout << "Product: " << product(values, number_value) << endl;
	cout << "Smallest: " << smallest(values, number_value) << endl;
	//! freeing up previously opened memory
	delete[]values;
	return 0;
}
/**
* Definition of the sum function:
* The sum function gets 2 variable, one of them is pointer for using memory correctly
* and the other one is number_value of the file. It calculates sum of these numbers and returns this value.
*/
float sum(int* values, int number_value) {
	int sum = 0;
	for (int i = 0; i < number_value; i++)
		sum += values[i];
	return sum;
}
/**
*  Definition of the average function:
*  The average function gets same variables with sum function and returns sum/number_value.
*/
float average(int* values, int number_value) {
	return sum(values, number_value) / number_value;
}
/**
* Definition of the product function:
* This function does multiplication by product value, which is equal to 1, and returns product.
*/
float product(int* values, int number_value) {
	int product = 1;
	for (int i = 0; i < number_value; i++)
		product *= values[i];
	return product;
}
/**
* Definition of the smallest function:
* This function assume that the first number of the elements is the smallest number of them and compare with all numbers.
* If the compared value is smaller than the first value, new smallest value is this value and this calculating continues
* up to end of the file. Finally returns the smallest value.
*/
float smallest(int* values, int number_value) {
	int smlst = values[0];
	for (int i = 1; i < number_value; i++) {
		if (values[i] < smlst)
			smlst = values[i];
	}
	return smlst;
}