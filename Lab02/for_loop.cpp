#include <iostream>
#include <cstdio>
// libraries
using namespace std;
int main() {
    // Complete the code.
    int a, b;
    // decleration of the variables that user is going to enter
    cin >> a >> b;
    // prompting values of variables from user
    string num[9] = { "one", "two", "three", "four", "five", "six","seven", "eight", "nine" };
    // string for the desired output
    for (int i = a; i <= b; i++) {  // for loop to writing numbers with letters between a and b
        if (i <= 9) {   // if the number, which is going to written, is less than 10
            cout << num[i - 1] << endl;
        }
        else {     // if the number, which is going to written, is greater than 9
            if (i % 2 == 0) {   // if it is even
                cout << "even" << endl;
            }
            else {  // if it is odd
                cout << "odd" << endl;
            }
        }
    }
    return 0;
    // return value of the main function
}
