#include <sstream>
#include <vector>
#include <iostream>
// libraries
using namespace std;
vector<int> parseInts(string str) { // decleration and defination of the function
    // takes a string
    vector<int> result;
    int i;
    char c;

    stringstream ss(str);

    while (ss >> i) {
        result.push_back(i);  
        ss >> c;
    }

    return result;
}

int main() {
    string str;
    cin >> str;
    vector<int> integers = parseInts(str);
    for (int i = 0; i < integers.size(); i++) {
        cout << integers[i] << "\n";
    }
    return 0;
}