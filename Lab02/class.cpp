#include <iostream>
#include <sstream>
// libraries
using namespace std;
class Student { // decleration of the class
private:
    int age;
    int standard;
    string first_name;
    string last_name;
    // private variables definations
public:
    Student() { // Constructer
        age = 0;
        standard = 0;
    }
    void set_age(int new_age) { // set age function
        age = new_age;
    }
    void set_standard(int new_standard) {   // set standard function
        standard = new_standard;
    }
    void set_first_name(string new_first_name) {    // set first name function
        first_name = new_first_name;
    }
    void set_last_name(string new_last_name) {  // set last name function
        last_name = new_last_name;
    }
    int get_age() { // get age function
        return age;
    }
    int get_standard() {    // get standard function
        return standard;
    }
    string get_last_name() {    // get last name function
        return last_name;
    }
    string  get_first_name() {  // get first name function
        return first_name;
    }
    string to_string() {    // printer function
        stringstream ss;
        char c = ',';
        ss << age << c << first_name << c << last_name << c << standard;
        return ss.str();
    }
};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}
