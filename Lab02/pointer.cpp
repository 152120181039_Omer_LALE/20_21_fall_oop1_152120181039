#include <stdio.h>
#include <stdlib.h>
// libraries
using namespace std;
void update(int* a, int* b) // decleration and defination of the function
{
    int remainder = *a; // decleration of the reminder value to remind old value of the *a
    *a = *a + *b;   // *a is sum of *a and *b
    if (remainder > * b)    // if *a is greater than *b
    {
        *b = remainder - *b;
    }
    else {  // if *b is greater than *a
        *b = *b - remainder;
    }
    // b is absolute value of subtract of *a(=reminder) and *b
}
int main()
{
    int a, b;
    int* pa = &a, * pb = &b;

    scanf_s("%d %d", &a, &b);
    update(pa, pb);
    printf("%d\n%d", a, b);
    return 0;
}