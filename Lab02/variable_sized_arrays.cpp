﻿#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
// libraries
using namespace std;
int main() {

    /*
        2 2
        3 1 5 4
        5 1 2 8 9 3     → n=2   query=2   k=3,5
        0 1
        1 3
    */

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int number_of_array, query, size_of_arrays, elements_of_arrays, x_index, y_index;   // decleration of variables
    cin >> number_of_array >> query;    // prompting number of array and query value
    vector<vector<int>> bigger; // creating a 2 dimansional vector to hold all values of all arrays
    for (int i = 0; i < number_of_array; i++) {
        cin >> size_of_arrays;  // prompting size of array for each array (according to number of array)
        vector<int> smaller;    // creating a 1 dimansional vector to hold whole elements of an array
        for (int j = 0; j < size_of_arrays; j++) {
            cin >> elements_of_arrays;  // prompting elements of arrays
            smaller.push_back(elements_of_arrays);  // push elements ,that taken upper line, to 1 dimansional vector
        }
        bigger.push_back(smaller);  // // push elements of 1 dimansional vector to 2 dimansional vector
    }
    for (int k = 0; k < query; k++) {
        cin >> x_index >> y_index;  // prompting x and y indexes that is wanted to get value
        cout << bigger[x_index][y_index] << endl;   // printing desired value then "<<endl;"
    }
    return 0;
    // return value of main function
}