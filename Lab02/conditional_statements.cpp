#include<iostream>
// library
using namespace std;
int main() {
    int n;
    // decleration of the variable that is user going to enter
    string num[10] = { "Greater than 9", "one", "two", "three", "four", "five", "six","seven", "eight", "nine" };
    // string for the desired output
    cin >> n;
    // prompting the value of the variable from user
    if (n > 9)  // if the value of n is greater than 9
        cout << num[0];
    else    // if the value of n is less than 9
        cout << num[n];
    return 0;
    // return value of the main function
}