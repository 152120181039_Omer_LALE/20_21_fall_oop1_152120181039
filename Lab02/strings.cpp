#include <iostream>
#include <string>
// libraries
using namespace std;
int main() {
    // abcd
    // ef
    // ebcd af
    string s1, s2;  // decleration of the strings
    cin >> s1 >> s2;    // prompting the strings from user
    int len1 = s1.length(); 
    int len2 = s2.length();
    // calculation lengths of the strings
    cout << len1 << " " << len2 << endl;    // printing length values
    cout << s1 << s2 << endl;   // printing strings without any space between them
    
    // final task is changing first characthers of the strings
    char reminder = s1[0];  // creating a char variable to not lost first char of the firs string

    s1[0] = s2[0];  // first strings first char sets second strings first char 
    s2[0] = reminder;   // second strings first char sets first strings first char

    cout << s1 << " " << s2 << endl;    // printing changed versions of the strings
    return 0;
    // return value of the main function
}