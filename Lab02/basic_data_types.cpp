#include <iostream>
#include <cstdio>
#include <iomanip>
// libraries
using namespace std;
int main() {
    // Complete the code.
    int a; long b; char c; float d; double e;   // decleration of the variables
    cin >> a >> b >> c >> d >> e;   // prompting the values from user
    cout << a << "\n" << b << "\n" << c << "\n";
    cout << fixed << setprecision(3) << d << "\n";  // 3 numbers after the comma
    cout << fixed << setprecision(9) << e << "\n";  // 9 numbers after the comma
    // printing
    return 0;   // return value of main function
}
