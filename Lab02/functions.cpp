#include <iostream>
#include <cstdio>
// libraries
using namespace std;
/*
Add `int max_of_four(int a, int b, int c, int d)` here.
*/
int max_of_four(int a, int b, int c, int d) {   // decleration and defination of the function
    /*
        takes 4 integer
        returns maximum value between these 4 numbers
    */
    int max = a;    // initial value assignment of the max variable
    if (b > max) {  // if b is greater than max value, new max value sets b
        max = b;
    }
    if (c > max) {  // if c is greater than max value, new max value sets c
        max = c;
    }
    if (d > max) {  // if d is greater than max value, new max value sets d
        max = d;
    }
    return max;
    //returns the value of max
}
int main() {
    int a, b, c, d;
    scanf("%d %d %d %d", &a, &b, &c, &d);
    int ans = max_of_four(a, b, c, d);
    printf("%d", ans);

    return 0;
}