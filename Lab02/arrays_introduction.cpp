#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
// libraries
using namespace std;
int main() {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    int number_of_integers;  // decleration of the n, that is number of integers
    cin >> number_of_integers;  // prompting value of variable from user
    int array[1000];  // decleration of the array, which stores the values
    for (int i = 0; i < number_of_integers; i++) {  // increased
        cin >> array[i];  // prompting values of the arrays from the user
    }
    for (int i = number_of_integers - 1; i >= 0; i--) { // decreased
        cout << array[i] << " ";  // printing the values of the array reversely
    }
    return 0;
    // return value of the main function
}
